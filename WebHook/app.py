from flask import Flask, jsonify, request

import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials

import functions as func

app = Flask(__name__)


#User information
@app.route('/user/<string:correo>')
def getUserInformation(correo):
    client = func.__sheets_conection()
    user_info = func._user_validation(client, correo)

    return jsonify({'chat': user_info})


#Ticket history
@app.route('/tickets')
def getTickets():
    client = func.__sheets_conection()
    tickets_history = func._ticket_history(client)
    ticketToStr = ';'.join([str(elem) for elem in tickets_history['Ticket_Number']])
    tickets_available = {'Tickets': ticketToStr}

    return jsonify({'chat': tickets_available})
 

#Search a ticket
@app.route('/tickets/<string:ticket_id>')
def getTicket(ticket_id):
    client = func.__sheets_conection()
    tickets = func._ticket_history(client)
    ticket_status = func._tickets_search(tickets, ticket_id)

    return jsonify({'chat': ticket_status})


#Create a ticket
@app.route('/new_ticket', methods=['POST'])
def newTicket():
    ticket = request.json['chat']['soportenumeroticket']
    solicitante = request.json['chat']['correologin']
    fecha = request.json['chat']['soportefecha']
    tiposolicitud = request.json['chat']['tiposoporte']
    prioridad = request.json['chat']['soporteprioridad']
    robot = request.json['chat']['robotnombre']
    descripcion = request.json['chat']['soportedescripcion']
    estado = 'Pendiente'
    resp = {'ticket_estado': estado}

    client = func.__sheets_conection()
    func._create_ticket(client, ticket, solicitante, fecha, tiposolicitud, prioridad, robot, descripcion, estado)

    return jsonify({'chat': resp})


#Schedule a bot
@app.route('/new_schedule', methods=['POST'])
def newSchedule():
    robot_id = request.json['chat']['robotseleccionado']
    solicitante = request.json['chat']['correologin']
    robot = request.json['chat']['robotnombre']
    fechaProgramacion = request.json['chat']['operacionfechaprogramacion']
    fechaProgramada = request.json['chat']['fechaprogramada']
    resp = {'schedule_estado': 'OK'}

    client = func.__sheets_conection()
    func._schedule_robots(client, robot_id, solicitante, robot, fechaProgramacion, fechaProgramada)

    return jsonify({'chat': resp})


#Bot monitoring
@app.route('/robot/<string:robot_id>')
def getRobotInformation(robot_id):
    client = func.__sheets_conection()
    robot_info = func._robot_monitoring(client, robot_id)

    return jsonify({'chat': robot_info})

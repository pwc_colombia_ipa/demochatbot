import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials


def __sheets_conection():
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name("API_google_sheets.json", scope)
    client = gspread.authorize(creds)

    return client


def _user_validation(client, correo):
    users_sheet = client.open('Demo MAPFRE').worksheet('Usuarios')
    users = users_sheet.get_all_records()
    users_df = pd.DataFrame(users)
    users_df['ID'] = users_df['ID'].astype(str)

    try:
        user_info = users_df[users_df['Email'] == correo.lower()]
        user_dict = user_info.to_dict('records')

        return user_dict[0]

    except IndexError:
        ticket_dict = {'ID': '', 
                        'Nombre': '',
                        'Email': '', 
                        'Incidente': '', 
                        'Control_cambios': '', 
                        'Propuesta': '', 
                        'Tickets': '', 
                        'Robots': ''
                    }

        return ticket_dict


def _ticket_history(client):
    tickets_sheet = client.open('Demo MAPFRE').worksheet('Tickets')
    tickets = tickets_sheet.get_all_records()
    ticket_history = pd.DataFrame(tickets)
    ticket_history['Ticket_Number'] = ticket_history['Ticket_Number'].astype(str)
    
    return ticket_history


def _tickets_search(df, ticket_number):
    try:
        ticket_df = df[df['Ticket_Number'] == ticket_number]
        ticket_dict = ticket_df.to_dict('records')

        return ticket_dict[0]

    except IndexError:
        ticket_dict = {'Ticket_Number': '', 
                        'Solicitante': '', 
                        'Fecha': '', 
                        'Tipo': '', 
                        'Prioridad': '', 
                        'Robot': '', 
                        'Descripcion': '', 
                        'Estado': '', 
                        'Fecha_atencion': '', 
                        'Tiempo_atencion': '',
                        'Fecha_solucion': '',
                        'Tiempo_solucion': ''
                    }
        
        return ticket_dict


def _create_ticket(client, ticket, solicitante, fecha, tipo, prioridad, robot, descripcion, estado):
    new_ticket = [ticket, solicitante, fecha, tipo, prioridad, robot, descripcion, estado]
    tickets_sheet = client.open('Demo MAPFRE').worksheet('Tickets')
    tickets = tickets_sheet.get_all_records()

    index = len(tickets) + 2
    tickets_sheet.insert_row(new_ticket, index)


def _schedule_robots(client, robot_id, solicitante, robot, fecha_today, fecha_prog):
    if robot_id == "1":
        ultima_fecha = "26/05/2020 09:40:00"
        t_exitosas = "152"
        t_fallidas = "10"
    elif robot_id == "2":
        ultima_fecha = "28/05/2020 09:40:00"
        t_exitosas = "199"
        t_fallidas = "19"
    else:
        ultima_fecha = "22/05/2020 09:40:00"
        t_exitosas = "185"
        t_fallidas = "25"

    new_schedule = [robot_id, solicitante, robot, fecha_today, fecha_prog, ultima_fecha, t_exitosas, t_fallidas]
    
    robots_sheet = client.open('Demo MAPFRE').worksheet('Robots')
    robots = robots_sheet.get_all_records()

    index = len(robots) + 2
    robots_sheet.insert_row(new_schedule, index)


def _robot_monitoring(client, robot_id):
    robots_sheet = client.open('Demo MAPFRE').worksheet('Robots')
    robots = robots_sheet.get_all_records()
    robots_df = pd.DataFrame(robots)

    robots_df['ID'] = robots_df['ID'].astype(str)
    robots_df['Transacciones_exitosas'] = robots_df['Transacciones_exitosas'].astype(str)
    robots_df['Transacciones_fallidas'] = robots_df['Transacciones_fallidas'].astype(str)

    robots_df = robots_df[robots_df['ID'] == robot_id]
    robots_df = robots_df.tail(1)
    robots_dict = robots_df.to_dict('records')

    return robots_dict[0]